# we_manage

#### 介绍
大学生家教平台后台管理系统

此作品主要描述一个大学生家教平台，虽然有一点四不像，功能也不太完善，但是是本人做的第一个比较完整的小项目。

主要目录：

1.普通用户端（在我的另一个仓库：

[戳我]: https://gitee.com/hmy001/we

）：

we：为前台界面

we下面的目录文件we_teach_back：是本作品基于express+mongoDB搭建的后台。

we目录下面的：mongoDB-database是本项目的数据库

2.管理员用户端：we_manage即本仓库

**注意：**

管理员用户端使用到的数据库与后台与普通用户端是同一个的。后台与数据库都放在我的另一个仓库下面，也就是普通用户端的残酷下面，点击上面地址可以跳转。

**项目的目录结构图：**

![](E:\VueProject\we_manage\目录结构.PNG)

普通用户端预览地址：http://we_teach.humianyuan.cn/#/

[戳我]: http://we_teach.humianyuan.cn/#/

管理员用户预览地址：http://we_teach_admin.humianyuan.cn/

[戳我]: http://we_teach_admin.humianyuan.cn/

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Customize configuration

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
