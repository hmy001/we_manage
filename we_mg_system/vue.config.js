const webpack = require("webpack");
module.exports = {
  configureWebpack: {
    resolve: {
      alias: {
        'assets': "@/assets", //静态资源文件夹
        // 'common': "@/common", //工具包文件夹
        'components': '@/components', //组件文件夹
        'network': '@/network', //网络请求文件夹
        'views': '@/views', //视图文件夹
        'util': '@/util' //工具类文件夹
      }
    },
    //支持jquery
    plugins: [
      new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery",
        "windows.jQuery": "jquery"
      })
    ]
  },
  //    // 配置跨域
  devServer: {
    //设置代理
    proxy: {
      "/api_hu66": {
        // 允许访问数据的计算机名称
        target: 'http://localhost:3000/',
        ws: true, //启用webSocket
        changeOrigin: true, //开启代理跨域
        pathRewrite: {
          // 重写api地址
          '^/api_hu66': ""
        }
      },
      "/api_location": {
        // 允许访问数据的计算机名称
        // target: 'http://api.51aidian.com/api/cha.php',
        // target: 'https://ip.ws.126.net/ipquery',
        target: 'http://apis.juhe.cn/ip/ipNew',
        ws: true, //启用webSocket
        changeOrigin: true, //开启代理跨域
        pathRewrite: {
          // 重写api地址
          '^/api_location': ""
        }
      },
      "/api_ip": {
        // 允许访问数据的计算机名称
        // target: 'http://api.51aidian.com/api/cha.php',
        target: 'http://pv.sohu.com/cityjson?ie=utf-8',
        // target: 'https://ip.dhcp.cn/?ip=',
        ws: true, //启用webSocket
        changeOrigin: true, //开启代理跨域
        pathRewrite: {
          // 重写api地址
          '^/api_ip': ""
        }
      },
      "/ipv4": {
        // 允许访问数据的计算机名称
        // target: 'http://api.51aidian.com/api/cha.php',
        target: 'http://localhost:3000/login/ipv4',
        // target: 'https://ip.dhcp.cn/?ip=',
        ws: true, //启用webSocket
        changeOrigin: true, //开启代理跨域
        pathRewrite: {
          // 重写api地址
          '^/ipv4': ""
        }
      },
      // '/socket.io': {
      //   target: 'http://localhost:3000',
      //   ws: true,
      //   changeOrigin: true
      // },
      //  'sockjs-node': {
      //   target: 'http://localhost:3000',
      //   ws: false,
      //   changeOrigin: true
      //  },
    },
    disableHostCheck: true
  }
  //   css: {
  //     extract: true, // 是否使用css分离插件 ExtractTextPlugin
  //     sourceMap: false, // 开启 CSS source maps?
  //     loaderOptions: {
  //         scss: {
  //           prependData: `
  //           @import "@/assets/scss/my.scss";
  //         `
  //         }
  //     }, // css预设器配置项
  //     requireModuleExtension: false // 启用 CSS modules for all css / pre-processor files.
  // },
}