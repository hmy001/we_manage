import Vue from 'vue'
import VueRouter from 'vue-router'
// import store from '.'
Vue.use(VueRouter)

// 解决报错
const originalPush = VueRouter.prototype.push
const originalReplace = VueRouter.prototype.replace
// push
VueRouter.prototype.push = function push (location, onResolve, onReject) {
  // if (onResolve || onReject) return originalPush.call(this, location, onResolve, onReject)
  return originalPush.call(this, location).catch(err => {return;})
}
// replace
VueRouter.prototype.replace = function replace (location, onResolve, onReject) {
  // if (onResolve || onReject) return originalReplace.call(this, location, onResolve, onReject)
  return originalReplace.call(this, location).catch(err => err)
}
import{setCookie,getCookie} from 'util/myCookie'//获取cookie

const routes = [{
    path: '/login',
    name: 'Login',
    component: () => import( /* webpackChunkName: "about" */ '../views/Login.vue')
  },
  {
    path:'/',
    redirect: '/home'
  },
  {
    path: '/home',
    name: 'Home',
    meta: {
      auth: true,
      title: 'home'
    },
    component: () => import( /* webpackChunkName: "about" */ '../views/Home.vue')
  },
  {
    path: '/resource_release',
    meta: {
      auth: true,
      title: 'resource release'
    },
    name: 'resource_release',
    component: () => import( /* webpackChunkName: "about" */ '../views/resource_release.vue')
  },
  {
    path: '/resource_manage',
    meta: {
      auth: true,
      title: 'resource manage'
    },
    name: 'resource_manage',
    component: () => import( /* webpackChunkName: "about" */ '../views/resource_manage.vue')
  },
  {
    path: '/edit',
    meta: {
      auth: true,
      title: 'Edit'
    },
    name: 'Edit',
    component: () => import( /* webpackChunkName: "about" */ '../views/edit/Edit.vue')
  },
  {
    path: '/write',
    meta: {
      auth: true,
      title: 'Write'
    },
    name: 'Write',
    component: () => import( /* webpackChunkName: "about" */ '../views/Write.vue')
  },
  {
    path:'/process',
    meta:{
      auth:true,
      title:'process'
    },
    name:'Process',
    component: () => import( /* webpackChunkName: "about" */ '../views/Process.vue')
  },
  {
    path:'/user_manage',
    meta:{
      auth:true,
      title:'user manage'
    },
    name:'user_manage',
    component: () => import( /* webpackChunkName: "about" */ '../views/user_manage.vue')
  },
  {
    name: '404',
    path: '/404',
    component: () => import('@/views/404/notFound.vue'),
    meta:{
      title:'404'
    }
    },
    {
      path: '*', // 此处需特别注意至于最底部
      redirect: '/404'
      },
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})
router.beforeEach((to, from, next) => {
  if (to.matched.some(m => m.meta.auth)) {
    // 对路由进行验证
    if (getCookie('user') != null) { // 已经登陆
      next() // 正常跳转到你设置好的页面
    } else {
      // 未登录则跳转到登陆界面，query:{ Rurl: to.fullPath}表示把当前路由信息传递过去方便登录后跳转回来；
      next({
        path: '/login',
      })
    }
  } else {
    next()
  }
})
export default router