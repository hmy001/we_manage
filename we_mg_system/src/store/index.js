import 'es6-promise/auto'
import Vue from 'vue'
import Vuex from 'vuex'

//模块化
import state from './state'
import mutations from './mutations'
import actions from './actions'
import getters from './getters'

//1.安装插件
Vue.use(Vuex)

//2.创建store对象

// es6对象写法
const store = new Vuex.Store({
  state,//相当于data，不过作用范围是全局的，数据能够实时变化
  getters,//相当于computed，不过也是全局的
  mutations,//相当于methods，不过不能实现异步方法，在组件中需要调用this.$store.commit('方法名'，payload)把函数操作提交到此处，、payload为参数
  actions,//用于处理异步函数，接收一个context参数，此参数也是一个函数，可以使用store的全部方法，可以通过context.commit来提交方法到mutation当中，action中的函数使用dispatch来提交，就如同mutation的commit一样
 
})

//3.挂载vue实例上

export default store