export default {
  changePublicNav(state, payload) {//改变公共导航条样式
    state.is_showPublicNav = payload
  },
  changeLateralNavanimate(state, payload) {//给侧边导航条增加动画效果
    // console.log("侧边栏宽度：",payload)
    state.lateralNavanimate = payload.positive;
    state.padding_left = 2+(payload.negative/3)
    console.log("state.padding_left:",state.padding_left)
  },
  changenavIndex(state, payload){
    state.nav_Index = payload;//改变nav index
    console.log("state.nav_Index:",state.nav_Index)
  },
  judgeisLogin(state,payload){
    state.isLogin = payload;
  }
}
