 //根据键获取对应cookie值、
 export function getCookie(key) {
   var arr, reg = new RegExp("(^| )" + key.trim() + "=([^;]*)(;|$)");
   if (arr = document.cookie.match(reg))
    {
      // console.log("arr[0]="+arr[0]+", arr1= "+arr[1]+", arr[2]="+arr[2]);
    return (arr[2]);} 
   else
     return null;
 }

 //设置cookie,增加到vue实例方便全局调用，key为键，value为值，expire_days为过期时间
 export function setCookie(key, value, expire_days) {
   var ex_date = new Date();
   ex_date.setDate(ex_date.getDate() + expire_days);
   document.cookie = key.trim() + "=" + escape(value.trim()) + ((expire_days == null) ? "" : ";expires=" + ex_date.toGMTString());
 };

 //根据键,删除对应cookie
 export function delCookie(key) {
   var exp = new Date();
   exp.setTime(exp.getTime() - 1);//设置时间为以前的时间就可以取消cookie
   var val = getCookie(key);
   if (val != null)
     document.cookie = key + "=" + val + ";expires=" + exp.toGMTString();
 };