export function getDistanceDay(time) {//计算回复时间与当前时间的差
	let stime = new Date().getTime();
	let usedTime = stime - time; //两个时间戳相差的毫秒数

	let one_minute = 60*1000;
	let one_hour = 60 * 60*1000;
	let one_day = 24 * 60 * 60*1000;
	let timeTxt = '';
	if (usedTime >= one_day) {
		//相差几天
		let disparityDay = parseInt(usedTime / one_day);

		timeTxt = disparityDay <= 1 ? disparityDay + ' day ago' : disparityDay + ' days ago';
		if (disparityDay > getMonthDay()) timeTxt = getDisparityMonth(disparityDay) <= 1 ? getDisparityMonth(disparityDay) + 'month' : getDisparityMonth(disparityDay) + 'months';

		if (disparityDay > getYearDay()) timeTxt = parseInt(disparityDay / getYearDay()) <= 1 ? parseInt(disparityDay / getYearDay()) + ' year ago' : parseInt(disparityDay / getYearDay()) + ' years ago';

	} else {
		if (usedTime >= one_hour) {
			timeTxt = parseInt(usedTime / one_hour) <= 1 ? parseInt(usedTime / one_hour) + ' hour ago' : parseInt(usedTime / one_hour) + ' hours ago';
		} else if (usedTime >= one_minute) {
			timeTxt = parseInt(usedTime / one_minute) <= 1 ? parseInt(usedTime / one_minute) + ' minute ago' : parseInt(usedTime / one_minute) + ' minutes ago';
		} else {
			timeTxt = 'just now';
		}
	}
	return timeTxt;
}

// 获取相差几个月 传天数
export function getDisparityMonth(disparityDay) {
	let disparityMonth = 0;
	let countFc = () => {
		if (disparityDay > getMonthDay(disparityMonth)) {
			disparityDay -= getMonthDay(disparityMonth)
			disparityMonth += 1;
			countFc(disparityMonth)
		} else {
			return disparityMonth;
		}
	}
	countFc(disparityMonth)
	return disparityMonth;
}

// 获取当前月
function getNowMonth() {
	return new Date().getMonth() + 1;
}

// 获取当前月有多少天 可以计算前面几个月有多少天 upNum 是前面几个月
export function getMonthDay(upNum) {
	let day = 0;
	let month = getNowMonth();
	if (upNum) {
		let date = new Date();
		date.setMonth(date.getMonth() - upNum);
		month = date.getMonth() + 1;
	}
	if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
		day = 31
	} else if (month == 2) {
		if (getYearDay() == 366) day = 29
		if (getYearDay() == 365) day = 28
	} else {
		day = 30
	}
	return day;
}

// 获取当前年有多少天
export function getYearDay() {
	let day = 365
	let year = new Date().getFullYear();
	if (year % 4 == 0) day = 366;
	return day
}




// export function calculateCP(time) {//传入评论或者回复的时间,生成计时器，实时计算
	
// 	let oneMinute = 60*1000;
// 	let oneHour = 60 * 60*1000;
// 	let oneDay = 24 * 60 * 60*1000;
// 	let minuteTimer = null; //分钟计时器
// 	let hourTimer = null; //小时计时器
// 	let dayTimer = null; //天数计时器
// 	let monthTimer = null; //月数计时器
// 	let yearTimer = null; //年份计时器

// 	if (true) {
// 		minuteTimer = setInterval(() => {
// 			if (new Date().getTime() - time < oneHour) { // 评论时间到现在小于1小时,那么1分钟计算1次评论的时间
// 				 getDistanceDay(time)
				
// 			} else {
// 				clearInterval(minuteTimer) //清除分钟计时器
// 				console.log("关闭分钟计时器")
// 				console.log("开启小时计时器")
// 				 getDistanceDay(time)
				
// 				hourTimer = setInterval(() => {
// 					if (new Date().getTime() - time <= oneDay) { // 评论时间到现在小于1天,那么1小时计算1次评论的时间
// 						 getDistanceDay(time)
						
// 					} else {
// 						clearInterval(hourTimer) //清除小时计时器 
// 						console.log("关闭小时计时器")
// 						console.log("开启天数计时器")
// 						 getDistanceDay(time)
				    
// 						dayTimer = setInterval(() => {
// 							if (new Date().getTime() - time <= 30 * oneDay) { // 评论时间到现在小于1个月,那么1天计算1次评论的时间
// 								 getDistanceDay(time)
								
// 							} else {
// 								let oneMonth = 30 * oneDay; //统一30天计算一次
// 								clearInterval(dayTimer) //清除天数计时器  
// 								console.log("关闭天数计时器")
// 								console.log("开启月数计时器")
// 								 getDistanceDay(time)
				        
// 								monthTimer = setInterval(() => {
// 									if (new Date().getTime() - time <= 365 * oneDay) { // 评论时间到现在小于1年,那么1个月计算1次评论的时间
// 										 getDistanceDay(time)
										
// 									} else {
// 										let oneyear = 365 * oneDay; //统一365天计算一次
// 										clearInterval(monthTimer) //清除天数计时器  
// 										console.log("关闭月数计时器")
// 										console.log("开启年数计时器")
// 										 getDistanceDay(time)
				            
// 										yearTimer = setInterval(() => { // 评论时间到现在大于1年,那么1年计算1次评论的时间
// 											 getDistanceDay(time)
											
// 										}, oneyear)
// 									}
// 								}, oneMonth)
// 							}
// 						}, oneDay)
// 					}
// 				}, oneHour)
// 			}
// 		}, oneMinute)
// 	}
// }

// let time = new Date().getTime()
// calculateCP(time)