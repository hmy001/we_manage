//debounce function,it used for delay the function execute,the first argument is the callback function and the other one is your delay time

export function debounce(callback,delay=500){
  let timer = null;
  return function(...args){//可以传递多个函数，使用return的目的是为了每一次触发debounce，都会返回
    if(timer!=null){clearTimeout(timer)}//如果连续给debounce传参，默认会等待delay时间后再执行
    timer = setTimeout(()=>{
       callback.apply(this,...args);//把callback函数的调用对象指向自己（return）返回的函数
       console.log("防抖……")
     },delay)
  }
}

