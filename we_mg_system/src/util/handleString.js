// 第一个参数文件名,第二个参数是生成唯一ID的函数
export function filter_fileNameStr(fileName,getUniqueID){// 过滤文件名称的中文及特殊字符 
   console.log("fileName:",fileName)
   var suffix = fileName.substring(fileName.lastIndexOf('.'), fileName.length)//后缀名
   console.log("后缀suffix:",suffix)
   var prefix = fileName.substring(0, fileName.lastIndexOf('.'))//截取后缀名之前的字符,以便于后面做字符处理 前缀名
   console.log("前缀prefix:",prefix)
   var reg = /^[\u4e00-\u9fa5\(\)_a-zA-Z0-9]+$/ //匹配中英文字符用于过滤特殊字符
   var reg_filterSpecial = /[^\u4e00-\u9fa5\(\)_a-zA-Z0-9]+/g //匹配非中英文的特殊字符,过滤特殊字符
   var reg_cn = /[\u4e00-\u9fa5\(\)]+/g //匹配中文字符用于过滤中文字符
   if(!reg.test(prefix)) {//匹配非所有中英文字符,
     console.log("文件含有特殊字符,开始过滤特殊字符,过滤前 前缀名称：",prefix)
     prefix = prefix.replace(reg_filterSpecial, '')//有特殊字符,则替换为空
     console.log("文件含有特殊字符,处理图片名称,过滤后 前缀名称：",prefix)
   }
   if(reg_cn.test(prefix)) {//匹配所有中英文字符,如果图片有中文字符,则替换为空
     console.log("文件含有中文,开始过滤中文,过滤前 前缀名称：",prefix)
     prefix = prefix.replace(reg_cn, '')//有中文字符,则替换为空
     console.log("文件含有中文,处理图片名称,过滤后 前缀名称：",prefix)
   }
   fileName = prefix+'_weteach_'+(getUniqueID().slice(1,10)) + suffix;//前缀加上唯一ID加上后缀名,重新命名完成 weteach是我作品的名称
   return fileName;
}