import axios from './myAxios'
// 资源管理模块:
// 资源发布
// 1.实时资源 或 政策发布
export function release_realtime_policy(type,id,title,content,tag){
  // 返回axios里面的request方法
  return axios.request({
    // method:"post",request,post
    url:"/resource_manage/saveResourceBytype",
    method:'post',
    data:{
      type,id,title,content,tag
    }
  })
}
// 2.课程资讯发布
export function release_subject(type,id,title,content,img,author,time,tag){
  // 返回axios里面的request方法
  return axios.request({
    // method:"post",request,post
    url:"/resource_manage/saveResourceBytype",
    method:'post',
    data:{
      type,id,title,content,img,author,time,tag
    }
  })
}
// 3.百科常识发布
export function release_nous(type,id,title,content,img,author,time){
  // 返回axios里面的request方法
  return axios.request({
    // method:"post",request,post
    url:"/resource_manage/saveResourceBytype",
    method:'post',
    data:{
      type,id,title,content,img,author,time
    }
  })
}
// 4.试题资源发布
export function release_test(type,id,title,content,tag){
  // 返回axios里面的request方法
  return axios.request({
    // method:"post",request,post
    url:"/resource_manage/saveResourceBytype",
    method:'post',
    data:{
      type,id,title,content,tag
    }
  })
}
// 5.学校资源发布
export function release_school(type,id,title,content,subcontent,tag){
  // 返回axios里面的request方法
  return axios.request({
    // method:"post",request,post
    url:"/resource_manage/saveResourceBytype",
    method:'post',
    data:{
      type,id,title,content,subcontent,tag
    }
  })
}
// 6.系统公告发布
export function release_notice(type,id,title,content,img,author,tag,time){
  // 返回axios里面的request方法
  return axios.request({
    // method:"post",request,post
    url:"/resource_manage/saveResourceBytype",
    method:'post',
    data:{
      type,id,title,content,img,author,tag,time
    }
  })
}

// 资源获取
// 1.按分类获取某个资源总数
export function getResourceCountBytype(type){
  // 返回axios里面的request方法
  return axios.request({
    // method:"post",request,post
    url:"/resource_manage/getResourceCountBytype",
    method:'post',
    data:{
      type
    }
  })
}
// 2.按分类和id获取某个资源
export function getResourceBy_type_id(type,_id){
  // 返回axios里面的request方法
  return axios.request({
    // method:"post",request,post
    url:"/resource_manage/getResourceBy_type_id",
    method:'post',
    data:{
      type,_id
    }
  })
}
// 3.按类型关键词查询资源
export function getResourceByKeyword(type,keyWord){
  // 返回axios里面的request方法
  return axios.request({
    // method:"post",request,post
    url:"/resource_manage/getResourceByKeyword",
    method:'post',
    data:{
      type,keyWord
    }
  })
}

// 4.按类型分页查询资源
export function getResourceBytype(type,page){
  // 返回axios里面的request方法
  return axios.request({
    // method:"post",request,post
    url:"/resource_manage/getResourceBytype",
    method:'post',
    data:{
      type,page
    }
  })
}

