import axios from './myAxios'
// 首页模块:
// 1.顶部模块数量
export function getTopPartNums(type){
  // 返回axios里面的request方法
  return axios.request({
    // method:"get",request,post
    url:"/admin_home",
    method:'get',
    params:{
      type
    }
  })
}
// 2.文章模块数量
export function getArticleLine(){
  // 返回axios里面的request方法
  return axios.request({
    // method:"get",request,post
    url:"/admin_home/articleLine",
    method:'get',
  })
}