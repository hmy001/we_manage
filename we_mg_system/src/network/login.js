import axios from './myAxios'
// 登录模块:
// 1.登录
export function Login(user,pass){
  // 返回axios里面的request方法
  return axios.request({
    // method:"get",request,post
    url:"/login/adminLogin",
    method:'post',
    data:{
      user,
      pass
    }
  })
}