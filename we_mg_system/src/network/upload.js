import axios from './myAxios'
// 上传模块:
//参数为id的值 username【用户名】 myStatus【用户身份】 key【文件名】
export function getUploadtokenByQiniu(username,myStatus,key){//获取七牛云token
  // 返回axios里面的request方法
  return axios.request({
    // method:"get",request
    url:"/upload/token",
    method:'get',
   params:{
    username,
    myStatus,
    key
   }
  })
}
