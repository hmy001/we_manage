import axios from 'axios'
import config from '@/config'//配置接口地址
// 判断是否上线还是开发中，选择对应的接口地址
const baseUrl = process.env.NODE_ENV == 'development' ? config.baseUrl.dev : config.baseUrl.pro
class HttpRequest{
  constructor(baseurl){
    this.baseUrl = baseUrl;
    // 请求队列
    this.queue = {}
  }
  // 用于基本配置
  getInsideConfig(){
    const config = {
      baseURL:this.baseUrl,
      header:{

      }
    }
    return config
  }
 //拦截设置
 interceptors(instance,url){
  instance.interceptors.request.use((config) => {
    // 拦截请求配置
    return config
  })
  instance.interceptors.response.use((res) => {
    // 处理响应数据
    return res.data
  },(error) => {
    console.log(error)
    return {error:"网络出错了"}
  })
}
//用于创建axios实例,并且配置一些参数
request(options){
  const instance = axios.create() //创建axios实例
  // 结合传入的options和getInsideConfig()配置
  options = Object.assign(this.getInsideConfig(),options)
  this.interceptors(instance,options.url)
  return instance(options)
}

}

// 实例化httpRequest对象
const axiosObj = new HttpRequest(baseUrl)
export default axiosObj