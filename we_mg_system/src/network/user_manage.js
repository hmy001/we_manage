import axios from './myAxios'
// 用户管理模块:
// 1.用户名单查询 参数为页码，类型
export function getUserList(page,type){
  // 返回axios里面的request方法
  return axios.request({
    // method:"get",request,get
    url:"/manageUser",
    method:'get',
    params:{
      page,type
    }
  })
}

// 2.移入黑名单 参数为账号 昵称 身份
export function insertBlack(_id,user,nickname,status){
  // 返回axios里面的request方法
  return axios.request({
    // method:"get",request,get
    url:"/manageUser/insertBlack",
    method:'get',
    params:{
      _id,user,nickname,status
    }
  })
}


// 3.移出黑名单 参数为唯一id
export function removeBlack(id,status){
  // 返回axios里面的request方法
  return axios.request({
    // method:"get",request,get
    url:"/manageUser/removeBlack",
    method:'get',
    params:{
      id,status
    }
  })
}


// 4.获取名单总量
export function total(type){
  // 返回axios里面的request方法
  return axios.request({
    // method:"get",request,get
    url:"/manageUser/total",
    method:'get',
    params:{
      type
    }
  })
}


// 5.模糊搜索
export function vagueSearch(keyWord,type){
  // 返回axios里面的request方法
  return axios.request({
    // method:"get",request,get
    url:"/manageUser/vagueSearch",
    method:'get',
    params:{
      keyWord,type
    }
  })
}

