import axios from './myAxios'
// 信息处理模块:
// 1.获取信息 参数为类型与页码
export function getProcessMsg(type,page){
  // 返回axios里面的request方法
  return axios.request({
    // method:"get",request,post
    url:"/msg_process",
    method:'get',
    params:{
      type,page
    }
  })
}
// 2.更新信息
export function updateMsg(type,_id,hasRead,is_recycle){
  // 返回axios里面的request方法
  return axios.request({
    // method:"get",request,post
    url:"/msg_process/updateMsg",
    method:'get',
    params:{
      type,_id,hasRead,is_recycle
    }
  })
}
// 3.删除信息
export function deleteMsg(type,_id){
  // 返回axios里面的request方法
  return axios.request({
    // method:"get",request,post
    url:"/msg_process/deleteMsg",
    method:'get',
    params:{
      type,_id
    }
  })
}

// 4.根据用户反馈或举报 回复用户
export function reply(title,content,to_id,to_status,report_id){
  // 返回axios里面的request方法
  return axios.request({
    // method:"get",request,post
    url:"/msg_process/reply",
    method:'get',
    params:{
      title,content,to_id,to_status,report_id
    }
  })
}