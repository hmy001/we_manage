import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
//引入css
import "./assets/css/normalize.css"
import "./assets/css/we_teach.css"
import "./assets/css/font.css"
//引入第三方组件库
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/display.css';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI)
import shnUI from 'shn-vue-ui'
Vue.use(shnUI);
import VueQuillEditor from 'vue-quill-editor' 
import 'quill/dist/quill.core.css' // import styles 
import 'quill/dist/quill.snow.css' // for snow theme%
import 'quill/dist/quill.bubble.css' // for bubble theme
Vue.use(VueQuillEditor)//编辑器
Vue.config.productionTip = false
//给vue加上总线事件$bus
Vue.prototype.$eventBus = new Vue()
// import echarts from 'echarts'//图形库
// Vue.prototype.$echarts = echarts
// import animated from "animate.css"
// Vue.use(animated)//使用animated实现动画效果
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
